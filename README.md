# issues-scala - Bugs, Issues, Questions

- [Bug: Scala 3.0.0 Standard Lib breaks code that uses JUnit 4.13 from Maven](scala3-library_3.0.0_breaks_junit_4.13/README.md)
