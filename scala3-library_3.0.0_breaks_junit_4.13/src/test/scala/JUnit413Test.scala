import org.junit.{Test,Assert}

class JUnit413Test {

  @Test
  def test_expect_throwable_with_message: Unit = {

    // only since junit 4.13:
    val t: Throwable = Assert.assertThrows(
      classOf[java.lang.IllegalArgumentException],
      () => throw new IllegalArgumentException("usage error")
    )
    Assert.assertEquals("usage error", t.getMessage());
  }

  // old style, deprecated in junit 4.13:
  @Test(expected=classOf[java.lang.IllegalArgumentException])
  def test_expect_throwable: Unit =
    throw new IllegalArgumentException("usage error")
}
