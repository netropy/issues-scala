# Bug: Scala 3.0.0 Standard Lib breaks code that uses JUnit 4.13 from Maven

Reporter: martin.zaun@gmx.com

### Summary:

1. ___Problem:___ Broken test-compile with _scala 3.0.0_ and _maven_ and
   _junit 4.13_; no problems with _sbt_ or any _scala 2.x_.
1. ___Cause:___ scala3-library\_3-3.0.0.{pom,jar} has an indirect dependency
   upon junit 4.11 via: com.novocode, junit-interface, version 0.11
1. When using scala 3.0.0 from maven via scala-maven-plugin, version 4.5.3,
   this junit 4.11 dependency ends up in the _bootclasspath_.
1. This junit 4.11 jar then shadows regular junit 4.13 dependency in the
   _classpath_, breaking compile of code that depends upon 4.13.x.
1. ___Workaround:___ not known; found no documentation on how to tweak
  scala-maven-plugin for the _bootclasspath_ in order to remove junit 4.11 or
  to prepend with 4.13.
1. ___Hack___: test-compile can be fixed by manually removing the
  _com.novocode/junit-interface_ dependency from
  _~/.m2/repository/.../scala3-library\_3-3.0.0.pom_,
  which clears junit 4.11 from the bootclasspath (while keeping 4.13 as
  regular dependency in the classpath).
1. ___Suggestion:___ Ideally, the Scala 3.0.0 Standard Lib would be
   self-contained; at least, remove (or make module-private) its dependency
   upon JUnit 4.11 (or upgrade the novocode junit-interface to JUnit 4.13).

indirect
dependency

### How to reproduce the test-compile failure

Probably any Scala 3.0.x and scala-maven-plugin 4.5.y will reproduce:
```
$ scala -version
Scala compiler version 3.0.0 -- Copyright 2002-2021, LAMP/EPFL

$ mvn -version
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: /usr/local/Cellar/maven/3.8.1/libexec
Java version: 11.0.11, vendor: AdoptOpenJDK, runtime: /Library/Java/JavaVirtualM
achines/adoptopenjdk-11.jdk/Contents/Home
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "11.4", arch: "x86_64", family: "mac"

$ grep \<scala-maven-plugin.version\> pom.xml
    <scala-maven-plugin.version>4.5.3</scala-maven-plugin.version>

$ sbt -version
sbt version in this project: 1.5.4
sbt script version: 1.5.3

$ uname -a
Darwin mzmac 20.5.0 Darwin Kernel Version 20.5.0: Sat May  8 05:10:33 PDT 2021;
root:xnu-7195.121.3~9/RELEASE_X86_64 x86_64
```

Confirm that test _src/test/scala/JUnit413Test.scala_ calls `assertThrows`,
which is a newly added method in:
[JUnit 4.13 Assert](https://junit.org/junit4/javadoc/4.13/org/junit/Assert.html#assertThrows(java.lang.Class,%20org.junit.function.ThrowingRunnable)):
```
    val t: Throwable = Assert.assertThrows(
	...
```

Run maven for scala3 test-compile with debug output, find junit 4.11 in
_bootclasspath_ shadowing junit 4.13 in _classpath_, see test-compile fail:
```
$ mvn -X clean test
...
[INFO] compiling 1 Scala source to /Users/mz/gitlab/issues-scala/scala3-library_
3.0.0_breaks_junit_4.13/target/test-classes ...
[DEBUG] Returning already retrieved and compiled bridge: /Users/mz/.m2/repositor
y/org/scala-lang/scala3-sbt-bridge/3.0.0/scala3-sbt-bridge-3.0.0.jar.
[DEBUG] [zinc] Running cached compiler 2589d787 for Scala Compiler version 3.0.0
[DEBUG] [zinc] The Scala compiler is invoked with:
	-bootclasspath
	/Users/mz/.m2/repository/org/scala-lang/scala3-library_3/3.0.0/scala3-librar
	y_3-3.0.0.jar:/Users/mz/.m2/repository/com/novocode/junit-interface/0.11/jun
	it-interface-0.11.jar:/Users/mz/.m2/repository/junit/junit/4.11/junit-4.11.j
	ar:/Users/mz/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3
	.jar:/Users/mz/.m2/repository/org/scala-sbt/test-interface/1.0/test-interfac
	e-1.0.jar:/Users/mz/.m2/repository/org/scala-lang/scala-library/2.13.5/scala
	-library-2.13.5.jar
	-classpath
	/Users/mz/gitlab/issues-scala/scala3-library_3.0.0_breaks_junit_4.13/target/
	test-classes:/Users/mz/.m2/repository/junit/junit/4.13.2/junit-4.13.2.jar:/U
	sers/mz/gitlab/issues-scala/scala3-library_3.0.0_breaks_junit_4.13/target/cl
	asses:/Users/mz/.m2/repository/org/scala-lang/scala3-compiler_3/3.0.0/scala3
	-compiler_3-3.0.0.jar:/Users/mz/.m2/repository/org/scala-lang/scala3-interfa
	ces/3.0.0/scala3-interfaces-3.0.0.jar:/Users/mz/.m2/repository/org/scala-lan
	g/scala3-library_3/3.0.0/scala3-library_3-3.0.0.jar:/Users/mz/.m2/repository
	/org/scala-lang/scala-library/2.13.5/scala-library-2.13.5.jar:/Users/mz/.m2/
	repository/org/scala-lang/tasty-core_3/3.0.0/tasty-core_3-3.0.0.jar:/Users/m
	z/.m2/repository/com/novocode/junit-interface/0.11/junit-interface-0.11.jar:
	/Users/mz/.m2/repository/junit/junit/4.11/junit-4.11.jar:/Users/mz/.m2/repos
	itory/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:/Users/mz/.m2/rep
	ository/org/scala-sbt/test-interface/1.0/test-interface-1.0.jar:/Users/mz/.m
	2/repository/org/scala-lang/modules/scala-asm/9.1.0-scala-1/scala-asm-9.1.0-
	scala-1.jar:/Users/mz/.m2/repository/org/scala-sbt/compiler-interface/1.3.5/
	compiler-interface-1.3.5.jar:/Users/mz/.m2/repository/com/google/protobuf/pr
	otobuf-java/3.7.0/protobuf-java-3.7.0.jar:/Users/mz/.m2/repository/org/scala
	-sbt/util-interface/1.3.0/util-interface-1.3.0.jar:/Users/mz/.m2/repository/
	org/jline/jline-reader/3.19.0/jline-reader-3.19.0.jar:/Users/mz/.m2/reposito
	ry/org/jline/jline-terminal/3.19.0/jline-terminal-3.19.0.jar:/Users/mz/.m2/r
	epository/org/jline/jline-terminal-jna/3.19.0/jline-terminal-jna-3.19.0.jar:
	/Users/mz/.m2/repository/net/java/dev/jna/jna/5.3.1/jna-5.3.1.jar
[ERROR] dotty.tools.xsbt.PositionBridge@430abcad: value assertThrows is not a
member of object org.junit.Assert
[ERROR] one error found
```

### Crosscheck: Delete _novocode_ dependency from scala3-library_3-3.0.0.pom

Try hack: delete from file
_~/.m2/repository/org/scala-lang/scala3-library\_3/3.0.0/scala3-library\_3-3.0.0.pom_:
```
        <dependency>
            <groupId>com.novocode</groupId>
            <artifactId>junit-interface</artifactId>
            <version>0.11</version>
            <scope>test</scope>
        </dependency>
```

Compile again and confirm that junit 4.11 no longer in _bootclasspath_ (while
regular 4.13 dependency is still in _classpath_):
```
$ mvn -X clean test
...
[INFO] compiling 1 Scala source to /Users/mz/gitlab/issues-scala/scala3-library_
3.0.0_breaks_junit_4.13/target/test-classes ...
[DEBUG] Returning already retrieved and compiled bridge: /Users/mz/.m2/repositor
y/org/scala-lang/scala3-sbt-bridge/3.0.0/scala3-sbt-bridge-3.0.0.jar.
[DEBUG] [zinc] Running cached compiler 28be7fec for Scala Compiler version 3.0.0
[DEBUG] [zinc] The Scala compiler is invoked with:
	-bootclasspath
	/Users/mz/.m2/repository/org/scala-lang/scala3-library_3/3.0.0/scala3-librar
    y_3-3.0.0.jar:/Users/mz/.m2/repository/org/scala-lang/scala-library/2.13.5/s
    cala-library-2.13.5.jar
	-classpath
	/Users/mz/gitlab/issues-scala/scala3-library_3.0.0_breaks_junit_4.13/target/
    test-classes:/Users/mz/.m2/repository/junit/junit/4.13.2/junit-4.13.2.jar:
    ...

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
```

### Crosscheck: Confirm that Scala 2 has no problem with JUnit 4.13

Delete the modified _scala3-library\_3/3.0.0/_ dependency:
```
$ rm -rf ~/.m2/repository/org/scala-lang/scala3-library_3/
```

Change this _pom.xml_ to compile with Scala 2:
```
    <scala.version>2.13.6</scala.version>
    <scala.compat.version>2.13</scala.compat.version>
    <scala.library>scala-library</scala.library>
```

Confirm lack of junit 4.11 in _bootclasspath_ and test-compile:
```
$ mvn -X clean test
...
[INFO] compiling 1 Scala source to /Users/mz/gitlab/issues-scala/scala3-library_
3.0.0_breaks_junit_4.13/target/test-classes ...
[DEBUG] Returning already retrieved and compiled bridge: /Users/mz/.sbt/1.0/zinc
/org.scala-sbt/org.scala-sbt-compiler-bridge_2.13-1.5.5-bin_2.13.6__55.0-1.5.5_
20210607T021730.jar.
[DEBUG] [zinc] Running cached compiler 20027c44 for Scala compiler version 2.13.
6
[DEBUG] [zinc] The Scala compiler is invoked with:
	-bootclasspath
	/Users/mz/.m2/repository/org/scala-lang/scala-library/2.13.6/scala-library-2
	.13.6.jar
	-classpath
	/Users/mz/gitlab/issues-scala/scala3-library_3.0.0_breaks_junit_4.13/target/
	test-classes:/Users/mz/.m2/repository/junit/junit/4.13.2/junit-4.13.2.jar:
    ...

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
```

### Crosscheck: Confirm that sbt has no problem with JUnit 4.13

See _build.sbt_ for Scala 3, sbt-junit-interface, and junit 4.13:
```
    scalaVersion := "3.0.0",

    // use sbt-junit-interface for running junit4 tests
    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test",

    // add junit 4.13 classes, not part of sbt-junit-interface (4.11)
    libraryDependencies += "junit" % "junit" % "4.13" % Test,
```

Run test:
```
$ sbt clean test
...
[info] Passed: Total 2, Failed 0, Errors 0, Passed 2
```

Note, however, that having JUnit 4.11 plus 4.13 in the _classpath_ is not
ideal.

### Discussion: Problem of Scala 3.0.x Std Lib or scala-maven-plugin 4.5.y ?

The scala-maven-plugin does nothing special except adding the dependencies in
_scala3-library\_3-3.0.0.pom_ to the JVM's _bootclasspath_.  Conceivably,
other build tools might do the same to ensure that the Scala Std Lib is never
shadowed by dependencies in the _classpath_.

Hence, I consider this more of Scala 3.0.x Std Lib than a scala-maven-plugin
4.5.y issue.

Ideally, the Scala Standard Lib would be self-contained (and limited to the
_scala..._ namespace) to avoid any conflicts with dependencies the classpath.

If this goal is not attainable, at least, remove or make module-private the
indirect dependency upon JUnit 4.11 from com.novocode, junit-interface (or
upgrade it to JUnit 4.13).

Thanks!
martin.zaun@gmx.com
