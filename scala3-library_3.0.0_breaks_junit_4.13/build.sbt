lazy val root = project
  .in(file("."))
  .settings(
    name := "scala3-with-junit-4_13",
    version := "0.1.0",

    scalaVersion := "3.0.0",
    //scalaVersion := "2.13.6",

    // use sbt-junit-interface for running junit4 tests
    //   https://github.com/sbt/junit-interface
    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test",

    // add junit 4.13 classes, not part of sbt-junit-interface (4.11)
    //   if missing: sbt test:compile
    //   [error] value assertThrows is not a member of object org.junit.Assert
    //   see https://junit.org/junit4/javadoc/4.13/org/junit/Assert.html
    libraryDependencies += "junit" % "junit" % "4.13" % Test,

    // -v verbosity=2
    // -a make junit-interface show stacktraces for junit AssertionErrors
    testOptions += Tests.Argument(TestFrameworks.JUnit, "-a", "-v"),
  )
